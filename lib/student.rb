class Student
  attr_accessor :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + ' ' + @last_name
  end

  def courses
    @courses
  end

  def enroll(course)
    @courses.each do |old_course|
      if course.conflicts_with?(old_course)
        raise 'error'
      end
    end
    @courses << course unless @courses.include?(course)
    course.students << self
  end

  def course_load
    hsh = Hash.new(0)
    @courses.each do |course|
      hsh[course.department] += course.credits
    end
    hsh
  end

end
